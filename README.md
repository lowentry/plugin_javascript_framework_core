**Low Entry Javascript Framework Core** was the plugin which contained the main functionality of the [Low Entry Javascript Framework][1].

The functionality of the framework has been reworked to be built on top of the Gatsby framework instead.

See the [Low Entry Javascript Framework][1] for more information on which plugins it has been converted to, and on how to use them.


[1]: https://packagist.org/packages/lowentry/javascript-framework
